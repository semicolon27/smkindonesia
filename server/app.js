const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cors = require('cors');
const logger = require('morgan');
const bodyParser = require('body-parser')
const session = require('express-session');
const cookieParser = require("cookie-parser");

const apiRouter = require('./routes/Route');

const app = express();
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cors({ origin: true, credentials: true }));
app.use(logger('dev'));
app.use(express.json());
app.use(cookieParser("secret"));
app.use('/', express.static(path.join(__dirname, '../build')));

app.use(session({
    key: 'nip',
    secret: 'secret',
    resave: false,
    saveUninitialized: true,
    cookie: {
        expires: 600000,
        httpOnly: false
    }
}));
app.use((req, res, next) => {
    if (req.cookies.nip && !req.session.nip) {
        res.clearCookie('nip');
    }
    next();
});
app.use('/api', apiRouter);
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../build/index.html'))
});
// catch 404 and forward to error handler
app.use((req, res, next) => {
    next(createError(404));
});
// error handler
app.use((err, req, res, next) => {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    res.status(err.status || 500);
    res.send(err.status);
});

module.exports = app;