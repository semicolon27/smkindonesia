'use strict';
module.exports = (sequelize, DataTypes) => {
    const guruMengajarMapel = sequelize.define('guruMengajarMapel', {
        id_gmm: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        id_mapel: {
            allowNull: false,
            autoIncrement: false,
            primaryKey: false,
            type: DataTypes.INTEGER,
        },
        nip: {
            allowNull: false,
            autoIncrement: false,
            primaryKey: false,
            type: DataTypes.STRING,
        },
    }, {
        timestamps: false,
        freezeTableName: true,
        tableName: 'guru_mengajar_mapel'
    });
    guruMengajarMapel.associate = function(models) {
        // associations can be defined here
    };
    return guruMengajarMapel;
};