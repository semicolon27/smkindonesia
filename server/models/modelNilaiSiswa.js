'use strict';
module.exports = (sequelize, DataTypes) => {
    const nilai_siswa = sequelize.define('nilai_siswa', {
        id_nilai: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        nis: DataTypes.INTEGER,
        id_mapel: DataTypes.INTEGER,
        nilai_uh: DataTypes.INTEGER,
        nilai_uts: DataTypes.INTEGER,
        nilai_uas: DataTypes.INTEGER,
    }, {
        timestamps: false,
        freezeTableName: true,
        tableName: 'nilai_siswa',
    });
    nilai_siswa.associate = function(models) {
        // associations can be defined here
    };
    return nilai_siswa;
};