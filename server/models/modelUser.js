'use strict';
module.exports = (sequelize, DataTypes) => {
    const user = sequelize.define('user', {
        user_id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        nip: {
            allowNull: true,
            autoIncrement: false,
            primaryKey: false,
            type: DataTypes.STRING,
        },
        nis: {
            allowNull: true,
            autoIncrement: false,
            primaryKey: false,
            type: DataTypes.STRING,
        },
        password: DataTypes.TEXT,
    }, {
        timestamps: false,
        freezeTableName: true,
        tableName: 'user'
    });
    user.associate = function(models) {
        // associations can be defined here
    };
    return user;
};