'use strict';
module.exports = (sequelize, DataTypes) => {
    const prodi = sequelize.define('prodi', {
        id_prodi: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        nama_prodi: DataTypes.STRING,
    }, {
        timestamps: false,
        freezeTableName: true,
        tableName: 'prodi'
    });
    prodi.associate = function(models) {
        // associations can be defined here
    };
    return prodi;
}