'use strict';
module.exports = (sequelize, DataTypes) => {
    const murid = sequelize.define('murid', {
        nis: {
            allowNull: false,
            autoIncrement: false,
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        nama: DataTypes.STRING,
        jk: DataTypes.INTEGER,
        id_kelas: DataTypes.INTEGER,
    }, {
        timestamps: false,
        freezeTableName: true
    });
    murid.associate = function(models) {
        // associations can be defined here
    };
    return murid;
};