'use strict';
module.exports = (sequelize, DataTypes) => {
    const kelas = sequelize.define('kelas', {
        id_kelas: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        id_prodi: {
            allowNull: false,
            autoIncrement: false,
            type: DataTypes.INTEGER,
        },
        nama_kelas: DataTypes.STRING,
        walikelas: DataTypes.STRING,
    }, {
        timestamps: false,
        freezeTableName: true,
        tableName: 'kelas'
    });
    kelas.associate = function(models) {
        // associations can be defined here
    };
    return kelas;
};