'use strict';
module.exports = (sequelize, DataTypes) => {
    const mapel = sequelize.define('mapel', {
        id_mapel: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        nama_mapel: DataTypes.STRING,
    }, {
        timestamps: false,
        freezeTableName: true,
        tableName: 'mapel',
    });
    mapel.associate = function(models) {
        // associations can be defined here
    };
    return mapel;
};