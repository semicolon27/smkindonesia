'use strict';
module.exports = (sequelize, DataTypes) => {
    const guru = sequelize.define('guru', {
        nip: {
            allowNull: false,
            autoIncrement: false,
            primaryKey: true,
            type: DataTypes.STRING,
        },
        nama: DataTypes.STRING,
        no_hp: DataTypes.STRING,
        email: DataTypes.STRING,
        alamat: DataTypes.TEXT,
    }, {
        timestamps: false,
        freezeTableName: true,
        tableName: 'guru'
    });
    guru.associate = function(models) {
        // associations can be defined here
    };
    return guru;
};