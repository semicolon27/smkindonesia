const express = require('express');
const router = express.Router();
const Murid = require('../controller/MuridController');
const Kelas = require('../controller/KelasController');
const mapel = require('../controller/MapelController');
const nilai_siswa = require('../controller/NilaiSiswaController');
const User = require('../controller/UserController');
const Guru = require('../controller/GuruController');

router.get('/', Murid.getMurid);
router.get('/murid/kelas/:id', Murid.getMurid);

router.get('/kelas', Kelas.getKelas);

router.get('/nilai', nilai_siswa.getNilaiSiswa);
router.get('/nilai/mapel/:id_mapel', nilai_siswa.getNilaiSiswaByMapel);
router.get('/nilai/siswa/:nis', nilai_siswa.getNilaiSiswaByNis);
router.post('/nilai', nilai_siswa.postNilai);
router.put('/nilai/:id', nilai_siswa.putNilai);

router.get('/mapel', mapel.getMapel);
router.get('/mapel/id/:id', mapel.getMapelById);

router.get('/logout', User.logout);
router.get('/profil/:nip', User.getProfile);
router.post('/guru/login', User.loginNip)
router.post('/siswa/login', User.loginNis)

router.get('/mapel/:nip', Guru.getMapelByGuru);
router.get('/guru', Guru.getGuru);
router.get('/guru/:nip', Guru.getGuruByNip);

module.exports = router;