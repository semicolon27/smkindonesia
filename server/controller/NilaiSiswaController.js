const nilai = require('../models/index').nilai_siswa;
const mapel = require('../models/index').mapel;
const murid = require('../models/index').murid;
const kelas = require('../models/index').kelas;

nilai.belongsTo(murid, { foreignKey: 'nis' })
nilai.belongsTo(mapel, { foreignKey: 'id_mapel' })
murid.belongsTo(kelas, { foreignKey: 'id_kelas' })
    // kelas.belongsTo(prodi, { foreignKey: 'id_prodi' })

exports.getNilaiSiswa = async(req, res, next) => {
    try {
        const data = await nilai.findAll({
            include: [mapel, {
                model: murid,
                include: [kelas]
            }]
        });
        if (data.length !== 0) {
            res.json({
                'status': 'OK',
                'messages': '',
                'data': data
            })
        } else {
            res.json({
                'status': 'ERROR',
                'messages': 'EMPTY',
                'data': data
            }, 500)
        }
    } catch (err) {
        res.json({
            'status': 'ERROR',
            'messages': err,
            'data': {}
        })
    }
};
exports.getNilaiSiswaByMapel = async(req, res, next) => {
    try {
        const data = await nilai.findAll({
            include: [mapel, {
                model: murid,
                include: [kelas]
            }],
            where: {
                'id_mapel': req.params.id_mapel,
            }
        });
        if (data.length !== 0) {
            res.json({
                'status': 'OK',
                'messages': '',
                'data': data
            })
        } else {
            res.json({
                'status': 'ERROR',
                'messages': 'EMPTY',
                'data': data
            }, 500)
        }
    } catch (err) {
        res.json({
            'status': 'ERROR',
            'messages': err,
            'data': {}
        })
    }
};

exports.getNilaiSiswaByNis = async(req, res, next) => {
    try {
        const data = await nilai.findAll({
            include: [mapel, {
                model: murid,
                include: [kelas]
            }],
            where: {
                'nis': req.params.nis,
            }
        });
        if (data.length !== 0) {
            res.json({
                'status': 'OK',
                'messages': '',
                'data': data
            })
        } else {
            res.json({
                'status': 'ERROR',
                'messages': 'EMPTY',
                'data': data
            }, 500)
        }
    } catch (err) {
        res.json({
            'status': 'ERROR',
            'messages': err,
            'data': {}
        })
    }
};

exports.postNilai = async(req, res, next) => {
    try {
        const {
            nis,
            id_mapel,
            nilai_uh,
            nilai_uts,
            nilai_uas
        } = req.body;
        const data = await nilai.create({
            nis,
            id_mapel,
            nilai_uh,
            nilai_uts,
            nilai_uas
        });
        if (data) {
            res.status(201).json({
                'status': 'OK',
                'messages': 'Mobil berhasil ditambahkan',
                'data': data,
            })
        }
    } catch (err) {
        res.status(400).json({
            'status': 'ERROR',
            'messages': err.message,
            'data': {},
        })
    }
};
exports.putNilai = async(req, res, next) => {
    try {
        const id = req.params.id
        const {
            nis,
            id_mapel,
            nilai_uh,
            nilai_uts,
            nilai_uas
        } = req.body;
        const result = await nilai.update({
            id_mapel,
            nilai_uh,
            nilai_uts,
            nilai_uas
        }, {
            where: {
                id_nilai: id
            }
        });
        if (result) {
            res.json({
                'status': 'OK',
                'messages': 'Mobil berhasil diupdate',
                'data': result,
            })
        }
    } catch (err) {
        res.status(400).json({
            'status': 'ERROR',
            'messages': err.message,
            'data': {},
        })
    }
};
// exports.deleteMobil = async(req, res, next) => {
//     try {
//         const id_mobil = req.params.id;
//         const result = await mobil.destroy({
//             where: {
//                 id_mobil: id_mobil
//             }
//         })
//         if (result) {
//             res.json({
//                 'status': 'OK',
//                 'messages': 'Mobil berhasil dihapus',
//                 'data': result,
//             })
//         }
//     } catch (err) {
//         res.status(400).json({
//             'status': 'ERROR',
//             'messages': err.message,
//             'data': {},
//         })
//     }
// };