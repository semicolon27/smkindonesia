const Sequelize = require("sequelize");
const user = require("../models/index").user;
const guru = require("../models/index").guru;
const murid = require("../models/index").murid;
const jwt = require('jsonwebtoken')
const Op = Sequelize.Op;

user.belongsTo(guru, { foreignKey: 'nip' })
user.belongsTo(murid, { foreignKey: 'nis' })
process.env.SECRET_KEY = 'awokawowkawok'

exports.loginNip = async(req, res) => {
    try {
        const login = await user.findOne({
            where: {
                [Op.and]: [{
                        nip: req.body.nip
                    },
                    {
                        password: req.body.password
                    },
                ]
            }
        })
        if (login) {
            let token = jwt.sign(login.toJSON(), process.env.SECRET_KEY, {
                expiresIn: 1440
            })
            res.send(token)
        } else {
            res.json({
                isLoggedIn: false,
                message: "NIP dan/atau Password Salah",
                data: {
                    nip: req.body.nip,
                    password: req.body.password
                }
            })
            res.end()
        }
    } catch (err) {
        res.json({
            status: 'err',
            message: `Terjadi Kesalahan Teknis, Mohon Coba Beberapa Saat Lagi atau Segere Melapor ke Teknisi \n${err.message}`
        })
    }
}

exports.loginNis = async(req, res) => {
    try {
        const login = await user.findOne({
            where: {
                [Op.and]: [{
                        nis: req.body.nis
                    },
                    {
                        password: req.body.password
                    },
                ]
            }
        })
        if (login) {
            let token = jwt.sign(login.toJSON(), process.env.SECRET_KEY, {
                expiresIn: 1440
            })
            res.send(token)
        } else {
            res.json({
                isLoggedIn: false,
                message: "NIS dan/atau Password Salah",
                data: login,
                coba: req.body
            })
            res.end()
        }
    } catch (err) {
        res.json({
            status: 'err',
            message: `Terjadi Kesalahan Teknis, Mohon Coba Beberapa Saat Lagi atau Segere Melapor ke Teknisi \n${err.message}`
        })
    }
}

exports.getLoginStatus = async(req, res, next) => {
    console.log(res.cookie)
    try {
        const data = await user.findAll({
            include: guru,
            where: {
                nip: req.session.nip
            }
        });
        if (data.length !== 0) {
            res.json({
                status: "OK",
                messages: "",
                data: data
            });
        } else {
            res.json({
                    status: "ERROR",
                    messages: "EMPTY",
                    data: data
                },
                500
            );
        }
    } catch (err) {
        res.json({
            status: "ERROR",
            messages: err.message,
            data: res.cookie,
            // cookie: req.session.cookie.
        });
    }
};

exports.getProfile = async(req, res, next) => {
    try {
        const data = await user.findOne({
            include: guru,
            where: {
                nip: req.params.nip
            }
        });
        if (data) {
            res.json({
                status: "OK",
                messages: "",
                data: data
            });
        } else {
            res.json({
                    status: "ERROR",
                    messages: "EMPTY",
                    data: data
                },
                500
            );
        }
    } catch (err) {
        res.json({
            status: "ERROR",
            messages: err.message,
            data: {}
        });
    }
};

exports.cekLogin = async(req, res) => {
    try {
        if (req.session.loggedin) {
            res.json({
                isLoggedIn: req.session.loggedin,
                user: req.session.username
            })
        } else {
            res.json({
                islogin: req.session.loggedin,
                message: "Anda Belum Login",
            })
        }
    } catch (err) {
        res.json({
            status: 'error',
            message: err.message
        })
    }
}

exports.logout = async(req, res) => {
    try {
        localStorage.clear()
    } catch (err) {
        console.log(err)
    }
}