const murid = require('../models/index').murid;
const kelas = require('../models/index').kelas;
const prodi = require('../models/index').prodi;

murid.belongsTo(kelas, { singular: 'kelas', foreignKey: 'id_kelas' })
kelas.belongsTo(prodi, { foreignKey: 'id_prodi' })

exports.getMurid = async(req, res, next) => {
    try {
        const data = await murid.findAll({
            include: [{
                model: kelas,
                include: [prodi]
            }]
        });
        if (data.length !== 0) {
            res.json({
                'status': 'OK',
                'messages': '',
                'data': data
            })
        } else {
            res.json({
                'status': 'ERROR',
                'messages': 'EMPTY',
                'data': data
            }, 500)
        }
    } catch (err) {
        res.json({
            'status': 'ERROR',
            'messages': err,
            'data': {}
        })
    }
};
exports.getMuridByKelas = async(req, res, next) => {
    try {
        const data = await murid.findAll({
            include: [{
                model: kelas,
                include: [prodi]
            }],
            where: {
                id_kelas: req.params.id
            }
        });
        if (data.length !== 0) {
            res.json({
                'status': 'OK',
                'messages': '',
                'data': data
            })
        } else {
            res.json({
                'status': 'ERROR',
                'messages': 'EMPTY',
                'data': data
            }, 500)
        }
    } catch (err) {
        res.json({
            'status': 'ERROR',
            'messages': err,
            'data': {}
        })
    }
};
exports.getNilaiMurid = async(req, res, next) => {
    try {
        const data = await murid.findAll({
            include: [{
                model: kelas,
                include: [prodi]
            }]
        });
        if (data.length !== 0) {
            res.json({
                'status': 'OK',
                'messages': '',
                'data': data
            })
        } else {
            res.json({
                'status': 'ERROR',
                'messages': 'EMPTY',
                'data': data
            }, 500)
        }
    } catch (err) {
        res.json({
            'status': 'ERROR',
            'messages': err,
            'data': {}
        })
    }
};
// exports.postMobil = async(req, res, next) => {
//     try {
//         const {
//             id_merek,
//             tipe,
//             desc,
//             harga
//         } = req.body;
//         const data = await mobil.create({
//             id_merek,
//             tipe,
//             desc,
//             harga: harga
//         });
//         if (data) {
//             res.status(201).json({
//                 'status': 'OK',
//                 'messages': 'Mobil berhasil ditambahkan',
//                 'data': data,
//             })
//         }
//     } catch (err) {
//         res.status(400).json({
//             'status': 'ERROR',
//             'messages': err.message,
//             'data': {},
//         })
//     }
// };
// exports.putMobil = async(req, res, next) => {
//     try {
//         const id_mobil = req.params.id;
//         const {
//             id_merek,
//             tipe,
//             desc,
//             harga
//         } = req.body;
//         const result = await mobil.update({
//             id_merek,
//             tipe,
//             desc,
//             harga: harga
//         }, {
//             where: {
//                 id_mobil: id_mobil
//             }
//         });
//         if (result) {
//             res.json({
//                 'status': 'OK',
//                 'messages': 'Mobil berhasil diupdate',
//                 'data': result,
//             })
//         }
//     } catch (err) {
//         res.status(400).json({
//             'status': 'ERROR',
//             'messages': err.message,
//             'data': {},
//         })
//     }
// };
// exports.deleteMobil = async(req, res, next) => {
//     try {
//         const id_mobil = req.params.id;
//         const result = await mobil.destroy({
//             where: {
//                 id_mobil: id_mobil
//             }
//         })
//         if (result) {
//             res.json({
//                 'status': 'OK',
//                 'messages': 'Mobil berhasil dihapus',
//                 'data': result,
//             })
//         }
//     } catch (err) {
//         res.status(400).json({
//             'status': 'ERROR',
//             'messages': err.message,
//             'data': {},
//         })
//     }
// };