const mapel = require('../models/index').mapel;

exports.getMapel = async(req, res, next) => {
    try {
        const data = await mapel.findAll();
        if (data.length !== 0) {
            res.json({
                'status': 'OK',
                'messages': '',
                'data': data
            })
        } else {
            res.json({
                'status': 'ERROR',
                'messages': 'EMPTY',
                'data': data
            }, 500)
        }
    } catch (err) {
        res.json({
            'status': 'ERROR',
            'messages': err,
            'data': {}
        })
    }
};

exports.getMapelById = async(req, res, next) => {
    try {
        const data = await mapel.findOne({
            where: {
                'id_mapel': req.params.id,
            }
        });
        if (data.length !== 0) {
            res.json({
                'status': 'OK',
                'messages': '',
                'data': data
            })
        } else {
            res.json({
                'status': 'ERROR',
                'messages': 'EMPTY',
                'data': data
            }, 500)
        }
    } catch (err) {
        res.json({
            'status': 'ERROR',
            'messages': err,
            'data': {}
        })
    }
};