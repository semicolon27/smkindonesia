const mapel = require('../models/index').mapel;
const guru = require('../models/index').guru;
const guruMengajarMapel = require('../models/index').guruMengajarMapel;

guruMengajarMapel.belongsTo(mapel, { foreignKey: 'id_mapel' })

exports.getGuru = async(req, res, next) => {
    try {
        const data = await guru.findAll();
        if (data.length !== 0) {
            res.json({
                'status': 'OK',
                'messages': '',
                'data': data
            })
        } else {
            res.json({
                'status': 'ERROR',
                'messages': 'EMPTY',
                'data': data
            }, 500)
        }
    } catch (err) {
        res.json({
            'status': 'ERROR',
            'messages': err,
            'data': {}
        })
    }
};

exports.getGuruByNip = async(req, res, next) => {
    try {
        const data = await guru.findAll({
            where: {
                nip: req.params.nip,
            }
        });
        if (data.length !== 0) {
            res.json({
                'status': 'OK',
                'messages': '',
                'data': data
            })
        } else {
            res.json({
                'status': 'ERROR',
                'messages': 'EMPTY',
                'data': data
            }, 500)
        }
    } catch (err) {
        res.json({
            'status': 'ERROR',
            'messages': err,
            'data': {}
        })
    }
};

exports.getMapelByGuru = async(req, res, next) => {
    try {
        const data = await guruMengajarMapel.findAll({
            include: mapel,
            where: {
                nip: req.params.nip,
            }
        });
        if (data.length !== 0) {
            res.json({
                'status': 'OK',
                'messages': '',
                'data': data
            })
        } else {
            res.json({
                'status': 'ERROR',
                'messages': 'EMPTY',
                'data': data
            }, 500)
        }
    } catch (err) {
        res.json({
            'status': 'ERROR',
            'messages': err,
            'data': {}
        })
    }
};