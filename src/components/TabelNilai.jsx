import React, { useState, useEffect } from 'react';
import MaterialTable from 'material-table';
import Axios from 'axios';
import { useParams } from 'react-router-dom'

const EditableTable = props => {
  const [nilai, setNilai] = useState([])
  const [mapel, setMapel] = useState({})
  const [daftarMapel, setDaftarMapel] = useState([])
  const [daftarKelas, setDaftarKelas] = useState([])
  const apiUrl = "http://localhost:7007/api/";
  const roundToTwo = num => {    
    let roundedString = num.toFixed(2);
    let rounded = Number(roundedString);
    return rounded
  }
  const lookupMapel = daftarMapel.reduce(function(acc, cur, i) {
    acc[cur.id_mapel] = cur.nama_mapel;

    return acc;
  }, {});
  console.log(lookupMapel)
  const lookupKelas = daftarMapel.reduce(function(acc, cur, i) {
    acc[cur.id_kelas] = cur.nama_kelas;

    return acc;
  }, {});
  const columns = [
    { title: 'NIS', field: 'nis', editable: 'onAdd' },
    { title: 'Nama', field: 'murid.nama' , editable: 'never' },
    // { title: 'Nama', field: 'nama', render: rowData => rowData.murid.nama, editable: 'never'},
    { title: 'Kelas', field: 'murid.kela.nama_kelas' , editable: 'never' },
    // { title: 'Kelas', field: 'id_kelas', render: rowData => rowData.murid.kela.nama_kelas, editable: 'never' },
    // { title: 'Mapel', field: 'mapel.nama_mapel' , editable: 'never' },
    // { title: 'Mapel', field: 'id_mapel', render: rowData => rowData.mapel.nama_mapel, lookup: lookupMapel,},
    { title: 'UH', field: 'nilai_uh', type: 'numeric' },
    { title: 'UTS', field: 'nilai_uts', type: 'numeric'},
    { title: 'UAS', field: 'nilai_uas', type: 'numeric'},
    // { title: 'Coba', field: 'ngga'},
  // { title: 'Nilai Akhir',field: 'nilai_akhir', render: rowData => <span>{roundToTwo((rowData.nilai_uh + rowData.nilai_uts + rowData.nilai_uas) / 3)}</span>},
    { title: 'Nilai Akhir', field: 'nilai_akhir', render: rowData =>{
      try{
        return roundToTwo((rowData.nilai_uh + rowData.nilai_uts + rowData.nilai_uas) / 3)
      }catch(err){
      }
    } , editable: 'never'},
  ]  
  const getNilai = async id => {
    // let url = await "http://localhost:7007/api/nilai/mapel/"+props.match.params.idMapel
    // console.log(url)
    let res = await Axios.get(`${apiUrl}nilai/mapel/${props.match.params.idMapel}`);
    let mapel = await Axios.get(`${apiUrl}mapel/id/${id}`);
    let allMapel = await Axios.get(`${apiUrl}mapel`);
    // let allKelas = await Axios.get(`${apiUrl}kelas`);
    setNilai(res.data.data)
    setMapel(mapel.data.data)
    setDaftarMapel(allMapel.data.data)
    // setDaftarMapel(allKelas.data.data)
  }
  useEffect(() => {
    getNilai(props.match.params.idMapel);
  }, []);
  console.log(mapel)
  return (
    <MaterialTable
      style={{backgroundColor: props.warna.content}}
      options={{
          headerStyle: {backgroundColor: props.warna.content}
      }}
      title={mapel.nama_mapel}
      // title="nilai"
      columns={columns}
      data={nilai}
      editable={{
        onRowAdd: newData =>
              Axios.post("http://localhost:7007/api/nilai", {
                nis:newData.nis, 
                id_mapel: props.match.params.idMapel, 
                nilai_uh: newData.nilai_uh, 
                nilai_uts: newData.nilai_uts, 
                nilai_uas: newData.nilai_uas
              }).then((req, res) => {
                  getNilai();
                  console.log(newData)
              }),
        onRowUpdate: (newData, oldData) =>
            Axios.put("http://localhost:7007/api/nilai/"+oldData.id_nilai, {
                nis:newData.nis, 
                id_mapel: props.match.params.idMapel, 
                nilai_uh: newData.nilai_uh, 
                nilai_uts: newData.nilai_uts, 
                nilai_uas: newData.nilai_uas
            }).then((req, res) => {
                getNilai();
            }),
        onRowDelete: oldData =>
            Axios.delete("http://localhost:7007/api/nilai/"+oldData.id_nilai).then((req, res) => {
                getNilai();
            }),
      }}
    />
  );
}

export default EditableTable;