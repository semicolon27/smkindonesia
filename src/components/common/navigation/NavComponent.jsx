import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom'
import AppBar from '@material-ui/core/AppBar';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MailIcon from '@material-ui/icons/Mail';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Switch from '@material-ui/core/Switch';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import StarBorder from '@material-ui/icons/StarBorder';
import Collapse from '@material-ui/core/Collapse';
import Axios from 'axios';
import jwt_decode from 'jwt-decode'

const drawerWidth = 240;

function NavComponent(props) {
  const apiUrl = "http://localhost:7007/api/"
  const { container } = props;
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = useState(false);
  const [profile, setProfile] = useState({});
  const [nilaiMapel, setNilaiMapel] = useState([]);

  useEffect(() => {
    getUser()
  }, []);
  
  const getUser = async () => {
    if(localStorage.usertoken){
      const token = await localStorage.usertoken
      const decoded = await jwt_decode(token)
      if(decoded.nip){
        const res = await Axios.get(apiUrl + "profil/" + decoded.nip)
        const mapel = await Axios.get(apiUrl + "mapel/" + decoded.nip)
        setProfile(res.data.data.guru)
        setNilaiMapel(mapel.data.data)
      }
    }else{
      console.log("no token")
    }
  }

  const logout = () => {
    localStorage.removeItem('usertoken');
    props.history.push("/login/siswa")
  }

  function handleDrawerToggle() {
    setMobileOpen(!mobileOpen);
  }
  const link = {
    textDecoration: 'none', 
    // color: '#6C6C6C' light
    color: props.warna.text
  }
  const menu = [
    {
      'text': 'Murid',
      'path': '/murid'
    },
    {
      'text': 'Kelas',
      'path': '/kelas'
    },
  ]
  const useStyles = makeStyles(theme => ({
    root: {
      display: 'flex',
    },
    avatar: {
      margin: 20,
      height: 60,
      width: 60,
    },
    drawer: {
      [theme.breakpoints.up('sm')]: {
        width: drawerWidth,
        flexShrink: 0,
      },
    },
    appBar: {
      // marginLeft: drawerWidth,
      zIndex: theme.zIndex.drawer + 1,
      backgroundColor: props.warna.navbar,
      [theme.breakpoints.up('sm')]: {
        // width: `calc(100% - ${drawerWidth}px)`,
      },
    },
    menuButton: {
      marginRight: theme.spacing(2),
      [theme.breakpoints.up('sm')]: {
        display: 'none',
      },
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
      width: drawerWidth,
      backgroundColor: props.warna.sidebar,
      // color: "#fff",
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
    
  }));
  const [open, setOpen] = React.useState(true);

  const handleClick = () => {
    setOpen(!open);
  }
  const classes = useStyles();
  const drawer = (
    <div>
      <div className={classes.toolbar} />
      <Divider style={{display: 'flex', flex: 1, flexDirection: 'column'}} />
        <div style={{alignSelf: 'center'}}>
          <center>
            <Avatar className={classes.avatar}  />
          </center>
        </div>
        <Typography>
          {profile.nama ? profile.nama : null}
        </Typography>
      <Divider />
      <List>
        {menu.map((v, index) => (
          <Link key={index} style={link} to={v.path}>
            <ListItem button key={v.text}>
              <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
              <ListItemText primary={v.text} />
          </ListItem>
        </Link>
        ))}
        <ListItem button onClick={handleClick}>
          <ListItemIcon>
            <InboxIcon />
          </ListItemIcon>
          <ListItemText primary="Nilai" />
          {open ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={open} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
          {nilaiMapel.map((v, i) => (  
            <Link key={i} style={link} to={"/nilai/" + v.id_mapel}>          
              <ListItem button style={{paddingLeft: theme.spacing(8),}}>
                <ListItemText primary={v.mapel.nama_mapel} />
              </ListItem>
            </Link>
          ))}
          {nilaiMapel.length === 0 ? (
            <ListItem button style={{paddingLeft: theme.spacing(8),}}>
              <ListItemText primary="Kosong" />
            </ListItem>
          ) : null }

          </List>
        </Collapse>
      </List>
      <Divider />
      <List>
        <Link style={link} onClick={logout}>
            <ListItem button>
              <ListItemIcon> <InboxIcon /></ListItemIcon>
              <ListItemText primary="Log Out" />
            </ListItem>
          </Link>
      </List>
    </div>
  );

  return (
    <>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap>
            SM<span style={{color: 'red'}}>KI</span>NDONESIA
          </Typography>
          <Switch
            onClick={() => props.themeSwitch()}
          />
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
      
      </>
      )
      }

export default NavComponent;