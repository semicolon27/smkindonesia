import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import { Redirect } from 'react-router-dom';
import Axios from 'axios';
import { Button } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
    overflowX: 'auto',
  },
  table: {
    minWidth: 650,
  },
}));


const LaporanNilaiComponent = props => {
  const [rows, setRows] = useState([])
  const [murid, setMurid] = useState({})
  const getData = async () => {
    let res = await Axios.get("http://localhost:7007/api/nilai/siswa/"+props.match.params.nis)
    setRows(res.data.data)
    setMurid({
      nis: res.data.data[0].nis,
      nama: res.data.data[0].murid.nama,
      kelas: res.data.data[0].murid.kela.nama_kelas,
    })
  }
  useEffect(() => {
    getData();
  }, []);
  const roundToTwo = num => {    
    let roundedString = num.toFixed(2);
    let rounded = Number(roundedString);
    return rounded
  }

  const logout = () => {
    localStorage.removeItem('usertoken');
    // window.location.reload();
     props.history.push(`/login/siswa`)
  }

  const keluar = () => {
    props.tipe === "murid" ? logout() : props.history.push(`/murid`)
  }
  const classes = useStyles();
  console.log(props)
  return (
      <Paper className={classes.root}>
      <Button onClick={keluar}>Keluar</Button>
      {/* {props.match.params.nis === props.nis ? null : <Redirect to="/login/siswa" />} */}
      <Typography variant="subtitle1" gutterBottom>{murid.nis}</Typography>
      <Typography variant="subtitle1" gutterBottom>{murid.nama}</Typography>
      <Typography variant="subtitle1" gutterBottom>{murid.kelas}</Typography>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell>Mapel</TableCell>
            <TableCell align="right">Nilai UH</TableCell>
            <TableCell align="right">Nilai UTS</TableCell>
            <TableCell align="right">Nilai UAS</TableCell>
            <TableCell align="right">Nilai Akhir</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map(row => (
            <TableRow key={row.id_nilai}>
              <TableCell component="th" scope="row">
                {row.mapel.nama_mapel}
              </TableCell>
              <TableCell align="right">{row.nilai_uh}</TableCell>
              <TableCell align="right">{row.nilai_uts}</TableCell>
              <TableCell align="right">{row.nilai_uas}</TableCell>
              <TableCell align="right">{roundToTwo((row.nilai_uh + row.nilai_uts + row.nilai_uas) / 3)}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
  );
}

export default LaporanNilaiComponent;