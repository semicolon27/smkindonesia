import React, { useState } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Axios from 'axios';
import { withRouter, Redirect } from 'react-router-dom';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        SMKINDONESIA
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  root: {
    height: "100vh"
  },
  image: {
    backgroundImage: "url(/assets/img/login.jpg)",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "center"
    // backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

const LoginComponent = props => {
  const apiUrl = "http://localhost:7007/api/"
  const classes = useStyles();
  const [userId, setUserId] = useState('');
  const [pass, setPass] = useState('');
  const [isLoading, setLoading] = useState(false);
  const [err, setErr] = useState({isErr: false, message: ''});
  const [loginStatus, setLoginStatus] = useState(false);

  const handleLogin = async () => {
    setLoading(true)
    let dataGuru = {
      nip: userId,
      password: pass
    }
    let dataSiswa = {
      nis: userId,
      password: pass
    }
    let res = await Axios.post(apiUrl+props.tipe+"/login", props.tipe === "guru" ? dataGuru : dataSiswa);
    console.log(props.tipe)
    try{
      await localStorage.setItem('usertoken', res.data)
      setLoading(false)
      setLoginStatus(true)
      return res.data
    }catch(err){
      console.log(err)
      setErr({isErr: true, message: res.data.message})
    }
  } 
  let id = props.tipe === "guru" ? "nip" : "nis"
  return (
    <Grid container component="main" className={classes.root}>
    {loginStatus && props.tipe === "guru" ? props.history.push("/") : loginStatus && props.tipe === "siswa" ? props.history.push("/laporan/" + userId) : null}
      
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <form className={classes.form} noValidate>
            <TextField
            onChange={e => setUserId(e.target.value)}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id={id}
              label={id.toUpperCase()}
              name={id}
              autoComplete={id}
              autoFocus
            />
            <TextField
            onChange={e => setPass(e.target.value)}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
            />
            {/* <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            /> */}
            <Button
            onClick={handleLogin}
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              {isLoading ? 'Loading...' : 'Log In'}
            </Button>
            <Grid container>
              <Grid item xs>
                {/* <Link href="#" variant="body2">
                  Forgot password?
                </Link> */}
                {err.isErr ? <span style={{color: 'red'}}>{err.message}</span> : ''}
              </Grid>
              {/* <Grid item>
                <Link href="#" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid> */}
            </Grid>
            <Box mt={5}>
              <Copyright />
            </Box>
          </form>
        </div>
      </Grid>
    </Grid>
  );
}

export default withRouter(LoginComponent);
