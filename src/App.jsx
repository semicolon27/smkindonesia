import React, { useState, useEffect } from 'react';
import './App.css';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import { makeStyles, createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import { NavComponent } from "./components/common/common";
import TabelMurid from "./components/TabelMurid";
import TabelKelas from './components/TabelKelas';
import TabelNilai from './components/TabelNilai';
import LoginComponent from './components/LoginComponent'; 
import GuruBeranda from './components/GuruBeranda'; 
import LaporanNilaiComponent from './components/LaporanNilaiComponent'
import jwt_decode from 'jwt-decode'
import Coba from './components/Coba';

const drawerWidth = 240;
const darkTheme = createMuiTheme({
  palette: {
    type: 'dark',
  },
});

const lightTheme = createMuiTheme({
  palette: {},
});

const warna = {
  gelap: {
    navbar: '#292d38',
    sidebar: '#202430',
    background: '#36474f',
    content: '#273238',
    text: '#fff',
    scrollThumb: '#292d38'
  },
  terang: {
    navbar: '#3f51b5',
    sidebar: '#fff',
    background: '#fff',
    content: '#fff',
    text: '#6C6C6C',
    scrollThumb: '#3f51b5'
  }
}

const App = props => {
  const [userLoggedIn, setUserLoggedIn] = useState("");
  const [tipe, setTipe] = useState("");
  const [token, setToken] = useState({});

  const getUser = async () => {
    if(localStorage.usertoken){
      const token = localStorage.usertoken
      const decoded = jwt_decode(token)
      // let resUser = await Axios.get(url+'login/status', {withCredentials: true});
      setUserLoggedIn(decoded.nip ? decoded.nip : decoded.nis ? decoded.nis : null);
      setTipe(decoded.nip === null ? "murid" : decoded.nis === null ? "guru" : "gagl");
      console.log(decoded)
      setToken(decoded)
    } 
  }

  useEffect(() => {
    getUser();
  }, []);

  const [isDark, setDark] = useState(true);
  let a = isDark && (props.location.pathname !== '/login/siswa' || props.location.pathname !== '/login/guru' || props.location.pathname !== '/laporan/123' ) ? warna.gelap : warna.terang
  const useStyles = makeStyles(theme => ({
    root: {
      display: 'flex',
    },
    appBar: {
      // marginLeft: drawerWidth,
      zIndex: theme.zIndex.drawer + 1,
      [theme.breakpoints.up('sm')]: {
        // width: `calc(100% - ${drawerWidth}px)`,
      },
    },
    menuButton: {
      marginRight: theme.spacing(2),
      [theme.breakpoints.up('sm')]: {
        display: 'none',
      },
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
      width: drawerWidth,
    },
    content: {
      flexGrow: 1,
      // backgroundColor: theme.palette.secondary.dark,
      // backgroundColor: theme.palette.background.default,
      padding: theme.spacing(3),
      [theme.breakpoints.up('sm')]: {
        marginLeft: drawerWidth,
      },
    },
    '@global': {
      '*::-webkit-scrollbar': {
        width: '6px',
        height: '6px',
        backgroundColor: '#F5F5F5'
      },
      '*::-webkit-scrollbar-track': {
        // '-webkit-box-shadow': 'inset 0 0 6px rgba(0,0,0,0.00)',
        '-webkit-box-shadow': 'inset 0 0 6px rgba(0,0,0,0.3)',
        'background-color': '#F5F5F5'
      },
      '*::-webkit-scrollbar-thumb': {
        backgroundColor: a.scrollThumb,
        // backgroundImage: '-webkit-gradient(linear, 0 0, 0 100%, color-stop(.5, rgba(255, 255, 255, .2)), color-stop(.5, transparent), to(transparent))'
      }
    }
  }));
  const classes = useStyles();
  const handleThemeSwitch = () => {
    setDark(!isDark)
  }
  let theme = isDark ? darkTheme : lightTheme
  console.log(token)
  console.log(userLoggedIn);
  return (
    <div className="App" style={{backgroundColor: a.background, height: '100vh'}}>
      {/* {userLoggedIn === "" && props.location.pathname !== '/login/guru' ? <Redirect to='/login/siswa' /> : null} */}
    <Switch>
      <Route exact path="/login/siswa" render={props => <LoginComponent {...props} warna={ a } theme={theme} tipe="siswa" />} />
      <Route exact path="/login/guru" render={props => <LoginComponent {...props} warna={ a } theme={theme} tipe="guru" />} />
    {tipe === "guru" ? 
      <ThemeProvider theme={theme} >
      <NavComponent warna={ a } themeSwitch={handleThemeSwitch} nip={userLoggedIn} />
      <main className={classes.content} style={{backgroundColor: a.background}}>
      <div className={classes.toolbar} />
      {/* <button onClick={ () => setDark(!isDark) }>Dark</button> */}
        {/* isi dengan konten */}
        <Route exact path="/" render={props => <GuruBeranda {...props} warna={ a } theme={theme} />} />
        <Route exact path="/murid" render={props => <TabelMurid {...props} warna={ a } theme={theme} />} />
        <Route exact path="/kelas" render={props => <TabelKelas {...props} warna={ a } theme={theme} />} />
        <Route exact path="/nilai/:idMapel" render={props => <TabelNilai {...props} key={props.match.params.idMapel} nip={userLoggedIn} warna={ a } theme={theme} />} />
        <Route exact path="/laporan/:nis" render={props => <LaporanNilaiComponent tipe="guru" {...props} warna={ a } theme={theme} />} />
        <Route exact path="/coba" render={props => <Coba {...props} warna={ a } theme={theme} />} />
      </main>
      </ThemeProvider>
      : tipe === "murid" ? 
        <Route exact path="/laporan/:nis" render={props => <LaporanNilaiComponent tipe="murid" nis={userLoggedIn} {...props} />} />
      :
      null
      }
      </Switch>
    </div>
  );
}

export default withRouter(App);
